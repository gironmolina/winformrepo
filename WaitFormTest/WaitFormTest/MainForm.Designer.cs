﻿namespace WaitFormTest
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnShowWait = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnShowWait
            // 
            this.btnShowWait.Location = new System.Drawing.Point(394, 259);
            this.btnShowWait.Name = "btnShowWait";
            this.btnShowWait.Size = new System.Drawing.Size(235, 53);
            this.btnShowWait.TabIndex = 0;
            this.btnShowWait.Text = "Show Wait Form";
            this.btnShowWait.UseVisualStyleBackColor = true;
            this.btnShowWait.Click += new System.EventHandler(this.btnShowWait_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1058, 617);
            this.Controls.Add(this.btnShowWait);
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MainForm";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnShowWait;
    }
}


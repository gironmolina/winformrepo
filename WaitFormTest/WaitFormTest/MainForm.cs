﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WaitFormTest
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void btnShowWait_Click(object sender, EventArgs e)
        {
            #region Rx Aproach

            //var frm = new WaitForm();

            //IObservable<Unit> observableThread = Observable.Start(() => Thread.Sleep(5000));
            //observableThread.ObserveOn(this).Subscribe(x =>
            //{
            //    frm.Close();
            //});

            //frm.ShowDialog();

            #endregion

            #region Standard Aproach

            var frm = new WaitForm();

            var thread = new Thread(() =>
            {
                BeginInvoke(new MethodInvoker(() => frm.ShowDialog()));
                Thread.Sleep(2000);
                InvokeAction(() => frm.Close());
            });
            thread.Start();

            #endregion
        }

        #region Async Task Aproach

        //private async void button1_Click(object sender, EventArgs e)
        //{
        //    // create and display modal form
        //    var modalForm = new SplashScreen();
        //    BeginInvoke((Action)(() => modalForm.ShowDialog()));

        //    // do your async background operation
        //    await DoSomethingAsync();

        //    // close the modal form
        //    modalForm.Close();
        //}

        #endregion

        private async Task DoSomethingAsync()
        {
            // example of some async operation....could be anything
            await Task.Delay(2000);
        }

        public void InvokeAction(MethodInvoker action)
        {
            if (InvokeRequired)
            {
                Invoke(action);
            }
            else
            {
                action();
            }
        }
    }
}

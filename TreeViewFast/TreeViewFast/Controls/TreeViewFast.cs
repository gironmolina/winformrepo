﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace TreeViewFast.Controls
{
    public class TreeViewFast : TreeView
    {
        private bool dblClick;
        public readonly Dictionary<int, TreeNode> TreeNodes = new Dictionary<int, TreeNode>();

        /// <summary>
        /// Load the TreeView with items.
        /// </summary>
        /// <typeparam name="T">Item type</typeparam>
        /// <param name="items">Collection of items</param>
        /// <param name="getId">Function to parse Id value from item object</param>
        /// <param name="getParentId">Function to parse parentId value from item object</param>
        /// <param name="getDisplayName">Function to parse display name value from item object. This is used as node text.</param>
        public void LoadItems<T>(IEnumerable<T> items, Func<T, int> getId, Func<T, string> getParentId, Func<T, string> getDisplayName)
        {
            BeginUpdate();

            // Clear view and internal dictionary
            Nodes.Clear();
            TreeNodes.Clear();

            // Load internal dictionary with nodes
            foreach (var item in items)
            {
                string displayName = getDisplayName(item);
                var node = new TreeNode { Name = displayName, Text = displayName, Tag = item };
                TreeNodes.Add(getId(item), node);
            }

            // Create hierarchy and load into view
            foreach (int id in TreeNodes.Keys)
            {
                var node = GetNode(id);
                var obj = (T)node.Tag;
                string parentId = getParentId(obj);

                if (!string.IsNullOrEmpty(parentId))
                {
                    foreach (var treeNode in Nodes.Cast<TreeNode>().Where(treeNode => treeNode.Name == parentId))
                    {
                        treeNode.Nodes.Add(node);
                        break;
                    }
                }
                else
                {
                    Nodes.Add(node);
                }
            }
            EndUpdate();
        }

        /// <summary>
        /// Get a handle to the object collection.
        /// This is convenient if you want to search the object collection.
        /// </summary>
        public IQueryable<T> GetItems<T>()
        {
            return TreeNodes.Values.Select(x => (T)x.Tag).AsQueryable();
        }

        /// <summary>
        /// Retrieve TreeNode by Id.
        /// Useful when you want to select a specific node.
        /// </summary>
        /// <param name="id">Item id</param>
        public TreeNode GetNode(int id)
        {
            return TreeNodes[id];
        }

        /// <summary>
        /// Retrieve item object by Id.
        /// Useful when you want to get hold of object for reading or further manipulating.
        /// </summary>
        /// <typeparam name="T">Item type</typeparam>
        /// <param name="id">Item id</param>
        /// <returns>Item object</returns>
        public T GetItem<T>(int id)
        {
            return (T)GetNode(id).Tag;
        }


        /// <summary>
        /// Get parent item.
        /// Will return NULL if item is at top level.
        /// </summary>
        /// <typeparam name="T">Item type</typeparam>
        /// <param name="id">Item id</param>
        /// <returns>Item object</returns>
        public T GetParent<T>(int id) where T : class
        {
            var parentNode = GetNode(id).Parent;
            return parentNode == null ? null : (T)Parent.Tag;
        }

        /// <summary>
        /// Retrieve descendants to specified item.
        /// </summary>
        /// <typeparam name="T">Item type</typeparam>
        /// <param name="id">Item id</param>
        /// <param name="deepLimit">Number of generations to traverse down. 1 means only direct children. Null means no limit.</param>
        /// <returns>List of item objects</returns>
        public List<T> GetDescendants<T>(int id, int? deepLimit = null)
        {
            var node = GetNode(id);
            var enumerator = node.Nodes.GetEnumerator();
            var items = new List<T>();

            if (deepLimit.HasValue && deepLimit.Value <= 0)
                return items;

            while (enumerator.MoveNext())
            {
                // Add child
                var childNode = (TreeNode)enumerator.Current;
                var childItem = (T)childNode.Tag;
                items.Add(childItem);

                // If requested add grandchildren recursively
                int? childDeepLimit = deepLimit - 1;
                if (deepLimit.HasValue && !(childDeepLimit > 0))
                    continue;

                int childId = int.Parse(childNode.Name);
                List<T> descendants = GetDescendants<T>(childId, childDeepLimit);
                items.AddRange(descendants);
            }
            return items;
        }

        #region Disable Double Click Expand

        protected override void OnMouseDown(MouseEventArgs e)
        {
            dblClick = e.Button == MouseButtons.Left && e.Clicks == 2;
        }

        protected override void OnBeforeExpand(TreeViewCancelEventArgs e)
        {
            if (e.Action == TreeViewAction.Expand) e.Cancel = dblClick;
        }

        #endregion

        public List<TreeNode> SearchNodes(string nodeName, List<TreeNode> listToExclude)
        {
            var list = new List<TreeNode>();
            List<TreeNode> tempUpMachines = TreeNodes.Values.Select(x => x).ToList();

            foreach (var node in listToExclude)
            {
                foreach (var upMachine in TreeNodes.Values)
                {
                    if (node.Name == upMachine.Name)
                    {
                        tempUpMachines.Remove(upMachine);
                        break;
                    }
                }
            }

            foreach (var node in tempUpMachines)
            {
                if (string.IsNullOrEmpty(nodeName))
                {
                    if (node.Name.Contains("MV"))
                        break;
                    list.Add(node);
                }

                else if (nodeName.Length <= node.Text.Length)
                {
                    string strKeyToLookFor = nodeName.ToLower();
                    string strToSearch = node.Text.ToLower().Substring(0, nodeName.Length);
                    int result = (nodeName.Length - strToSearch.Replace(strKeyToLookFor, string.Empty).Length)/
                                 strKeyToLookFor.Length;

                    if (result > 0)
                        list.Add(node);
                }
            }
            return list;
        }
    }
}

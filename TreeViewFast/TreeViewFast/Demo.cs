﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using TreeViewFast.DomainServices;
using TreeViewFast.Entities;
using TreeViewFast.Extenders;

namespace TreeViewFast
{
    public partial class Demo : Form
    {
        private static string lastSearch = "";
        private List<UpMachines> upMachines;
        private readonly Timer timer = new Timer { Interval = 500 };

        public Demo()
        {
            InitializeComponent();
        }

        private void Demo_Load(object sender, EventArgs e)
        {
            treeViewOrigin.TreeViewNodeSorter = new TreeNodeAlphanumComparator();
            treeViewDestination.TreeViewNodeSorter = new TreeNodeAlphanumComparator();
            treeViewOrigin.Sorted = true;
            treeViewDestination.Sorted = true;
            timer.Tick += timer_Tick;
        }

        #region Methods

        private void LoadItems(Controls.TreeViewFast treeViewToLoad)
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                lblFeedbackFast.Text = "Please wait for TreeViewFast to load ...";

                // Parse count
                int itemCount = int.Parse(txtItemCount.Text);

                // Retrieve employees
                upMachines = UpMachineGenerator.GetUpMachines(itemCount);

                // Load items into both TreeViews
                LoadTreeView(upMachines, treeViewToLoad);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "LoadItems", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void LoadTreeView(IEnumerable<UpMachines> listUpMachines, Controls.TreeViewFast treeViewToLoad)
        {
            //Define functions needed by the load method
            var start = DateTime.Now;
            Func<UpMachines, int> getId = x => x.Id;
            Func<UpMachines, string> getParentId = x => x.ManagerId;
            Func<UpMachines, string> getDisplayName = x => x.Name;

            // Load items into TreeViewFast
            treeViewToLoad.LoadItems(listUpMachines, getId, getParentId, getDisplayName);
            treeViewToLoad.ExpandAll();
            RemoveEmptyNodes(treeViewToLoad);
            treeViewToLoad.Nodes[0].EnsureVisible();

            var elapsed = DateTime.Now.Subtract(start);
            lblFeedbackFast.Text = string.Format("TreeViewFast: {0:N0} ms ({1})", elapsed.TotalMilliseconds, elapsed.Display());
        }

        #endregion

        #region Event handlers

        private void btnLoad_Click(object sender, EventArgs e)
        {
            treeViewDestination.Nodes.Clear();
            LoadItems(treeViewOrigin);
        }

        #endregion

        private void TreeView_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            var treeViewSelected = ((Control)sender).Name == treeViewOrigin.Name ? treeViewOrigin : treeViewDestination;
            var hitTest = treeViewSelected.HitTest(e.Location);
            if (hitTest.Location != TreeViewHitTestLocations.Label)
                return;

            var selectedNode = (TreeNode)e.Node.Clone();
            bool isParentNode = selectedNode.Nodes.Count > 0;

            MoveSelectedNodeTo(isParentNode, selectedNode, treeViewSelected == treeViewOrigin ? treeViewDestination : treeViewOrigin);

            e.Node.Remove();
            e.Node.Nodes.Clear();
            
            RemoveEmptyNodes((TreeView)sender);
        }

        private void MoveSelectedNodeTo(bool isParentNode, TreeNode selectedNode, TreeView toTreeView)
        {
            var nodeInfo = (UpMachines)selectedNode.Tag;

            if (isParentNode)
            {
                if (toTreeView.Nodes.ContainsKey(nodeInfo.Name))
                {
                    foreach (var treeNode in toTreeView.Nodes.Cast<TreeNode>().Where(treeNode => treeNode.Name == nodeInfo.Name))
                    {
                        foreach (TreeNode node in selectedNode.Nodes)
                        {
                            treeNode.Nodes.Add(node);
                        }
                        break;
                    }
                }
                else
                {
                    toTreeView.Nodes.Add(selectedNode);
                }
            }
            else
            {
                if (toTreeView.Nodes.ContainsKey(nodeInfo.ManagerId))
                {
                    foreach (var treeNode in toTreeView.Nodes.Cast<TreeNode>().Where(treeNode => treeNode.Name == nodeInfo.ManagerId))
                    {
                        treeNode.Nodes.Add(selectedNode);
                        break;
                    }
                }
                else
                {
                    foreach (var machines in upMachines)
                    {
                        if (machines.Name != nodeInfo.ManagerId)
                            continue;

                        var node = new TreeNode { Name = machines.Name, Text = machines.Name, Tag = machines };
                        toTreeView.Nodes.Add(node);
                        node.Nodes.Add(selectedNode);
                        break;
                    }
                }
            }

            toTreeView.ExpandAll();
        }

        private void RemoveEmptyNodes(TreeView currentTreeView)
        {
            for (int i = currentTreeView.Nodes.Count - 1; i >= 0; i--)
            {
                if (currentTreeView.Nodes[i].Nodes.Count == 0)
                    currentTreeView.Nodes[i].Remove();
            }

            EnableControls();
        }

        private void EnableControls()
        {
            btnClear.Enabled = treeViewDestination.Nodes.Count > 0;

            if (treeViewOrigin.Nodes.Count > 0)
            {
                chkBoxSelectAll.CheckedChanged -= chkBoxSelectAll_CheckedChanged;
                chkBoxSelectAll.Checked = false;
                chkBoxSelectAll.CheckedChanged += chkBoxSelectAll_CheckedChanged;
            }
            else
            {
                chkBoxSelectAll.CheckedChanged -= chkBoxSelectAll_CheckedChanged;
                chkBoxSelectAll.Checked = true;
                chkBoxSelectAll.CheckedChanged += chkBoxSelectAll_CheckedChanged;
            }
        }

        private void TxtBoxSearch_TextChanged(object sender, EventArgs e)
        {
            timer.Stop();
            timer.Start();
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                ClearSearch();
                btnClear.Enabled = false;
                treeViewDestination.Nodes.Clear();
                LoadTreeView(upMachines, treeViewOrigin);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "LoadFast", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void ClearSearch()
        {
            txtBoxSearch.TextChanged -= TxtBoxSearch_TextChanged;
            txtBoxSearch.Text = "";
            txtBoxSearch.TextChanged += TxtBoxSearch_TextChanged;
        }

        private void chkBoxSelectAll_CheckedChanged(object sender, EventArgs e)
        {
            Cursor = Cursors.WaitCursor;
            var selectedTreeView = chkBoxSelectAll.Checked ? treeViewOrigin : treeViewDestination;
            if (selectedTreeView == treeViewOrigin)
            {
                selectedTreeView.Nodes.Clear();
                LoadTreeView(upMachines, treeViewDestination);
            }
            else
            {
                selectedTreeView.Nodes.Clear();
                LoadTreeView(upMachines, treeViewOrigin);
            }
            ClearSearch();
            Cursor = Cursors.Default;

        }

        private void timer_Tick(object sender, EventArgs e)
        {
            timer.Stop();
            SearchNode();
        }

        private void SearchNode()
        {
            Cursor = Cursors.WaitCursor;
            treeViewOrigin.BeginUpdate();

            try
            {
                if (lastSearch.Length > 0 && !txtBoxSearch.Text.ToLower().Contains(lastSearch.ToLower()))
                {
                    if (treeViewDestination.Nodes.Count > 0)
                    {
                        List<UpMachines> tempUpMachines = upMachines.Select(x => x).ToList();
                        List<TreeNode> destinationNodes = EnumerateAllNodes(treeViewDestination).Where(i => i.Nodes.Count == 0).ToList();
                        foreach (var node in destinationNodes)
                        {
                            var result = upMachines.First(s => s.Name == node.Name);
                            tempUpMachines.Remove(result);
                        }
                        LoadTreeView(tempUpMachines, treeViewOrigin);
                    }
                    else
                    {
                        LoadTreeView(upMachines, treeViewOrigin);
                    }
                }
            }
            finally
            {
                lastSearch = txtBoxSearch.Text;
            }

            if (txtBoxSearch.Text.Length > 0)
            {
                for (int i = treeViewOrigin.Nodes.Count; i > 0; i--)
                {
                    NodeFiltering(treeViewOrigin.Nodes[i - 1], txtBoxSearch.Text);
                }
            }
            treeViewOrigin.EndUpdate();
            Cursor = Cursors.Default;
        }

        private bool NodeFiltering(TreeNode node, string txt)
        {
            bool result = false;


            if (node.Nodes.Count == 0)
            {
                if (node.Text.ToLower().Contains(txt.ToLower()))
                {
                    result = true;
                }
                else
                {
                    node.Remove();
                }
            }
            else
            {
                if (node.Text.ToLower().Contains(txt.ToLower()))
                {
                    return true;
                }
                for (int i = node.Nodes.Count; i > 0; i--)
                {
                    if (NodeFiltering(node.Nodes[i - 1], txt))
                        result = true;
                }

                if (!result)
                    node.Remove();
            }
            return result;
        }

        private IEnumerable<TreeNode> EnumerateAllNodes(TreeView treeView)
        {
            return treeView.Nodes.Cast<TreeNode>().SelectMany(GetNodeBranch);
        }

        private IEnumerable<TreeNode> GetNodeBranch(TreeNode node)
        {
            yield return node;

            foreach (TreeNode child in node.Nodes)
                foreach (var childChild in GetNodeBranch(child))
                    yield return childChild;
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            ClearSearch();
            SearchNode();
        }
    }
}

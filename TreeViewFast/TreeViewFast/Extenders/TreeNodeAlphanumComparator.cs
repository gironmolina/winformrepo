﻿using System.Collections;
using System.Windows.Forms;

namespace TreeViewFast.Extenders
{
    public class TreeNodeAlphanumComparator : IComparer
    {
        public int Compare(object x, object y)
        {
            var tx = (TreeNode)x;
            var ty = (TreeNode)y;

            // Compare the length of the strings, returning the difference.
            if (tx.Text.Length != ty.Text.Length)
                return tx.Text.Length - ty.Text.Length;

            // If they are the same length, call Compare.
            return string.CompareOrdinal(tx.Text, ty.Text);
        }
    }
}

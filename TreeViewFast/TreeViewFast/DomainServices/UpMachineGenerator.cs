﻿using System;
using System.Collections.Generic;
using TreeViewFast.Entities;

namespace TreeViewFast.DomainServices
{
    public static class UpMachineGenerator
    {
        private static readonly Random MyRandom = new Random(DateTime.Now.Millisecond);

        public static List<UpMachines> GetUpMachines(int count)
        {
            var upMachines = new List<UpMachines>();
            for (int i = 0; i < count; i++)
            {
                var item = new UpMachines
                {
                    Id = i,
                    ManagerId = i > 20 ? "UP_01020." + MyRandom.Next(0, 21) : string.Empty,
                    Name = i > 20 ? "MV_01020." + i : "UP_01020." + i
                };
                upMachines.Add(item);
            }
            return upMachines;
        }
    }
}

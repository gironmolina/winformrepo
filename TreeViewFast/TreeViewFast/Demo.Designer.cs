﻿namespace TreeViewFast
{
    partial class Demo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnLoad = new System.Windows.Forms.Button();
            this.lblItemCount = new System.Windows.Forms.Label();
            this.txtItemCount = new System.Windows.Forms.TextBox();
            this.tableMainLayout = new System.Windows.Forms.TableLayoutPanel();
            this.pnlLoad = new System.Windows.Forms.Panel();
            this.lblSearch = new System.Windows.Forms.Label();
            this.txtBoxSearch = new System.Windows.Forms.TextBox();
            this.chkBoxSelectAll = new System.Windows.Forms.CheckBox();
            this.btnClear = new System.Windows.Forms.Button();
            this.lblFeedbackFast = new System.Windows.Forms.Label();
            this.treeViewOrigin = new TreeViewFast.Controls.TreeViewFast();
            this.treeViewDestination = new TreeViewFast.Controls.TreeViewFast();
            this.btnDelete = new System.Windows.Forms.Button();
            this.tableMainLayout.SuspendLayout();
            this.pnlLoad.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnLoad
            // 
            this.btnLoad.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnLoad.Location = new System.Drawing.Point(708, 11);
            this.btnLoad.Name = "btnLoad";
            this.btnLoad.Size = new System.Drawing.Size(75, 23);
            this.btnLoad.TabIndex = 1;
            this.btnLoad.Text = "Load";
            this.btnLoad.UseVisualStyleBackColor = true;
            this.btnLoad.Click += new System.EventHandler(this.btnLoad_Click);
            // 
            // lblItemCount
            // 
            this.lblItemCount.AutoSize = true;
            this.lblItemCount.Location = new System.Drawing.Point(15, 16);
            this.lblItemCount.Name = "lblItemCount";
            this.lblItemCount.Size = new System.Drawing.Size(121, 13);
            this.lblItemCount.TabIndex = 2;
            this.lblItemCount.Text = "Number of items to load:";
            // 
            // txtItemCount
            // 
            this.txtItemCount.Location = new System.Drawing.Point(140, 13);
            this.txtItemCount.Name = "txtItemCount";
            this.txtItemCount.Size = new System.Drawing.Size(100, 20);
            this.txtItemCount.TabIndex = 3;
            this.txtItemCount.Text = "100";
            // 
            // tableMainLayout
            // 
            this.tableMainLayout.ColumnCount = 2;
            this.tableMainLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableMainLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableMainLayout.Controls.Add(this.pnlLoad, 0, 0);
            this.tableMainLayout.Controls.Add(this.lblFeedbackFast, 0, 1);
            this.tableMainLayout.Controls.Add(this.treeViewOrigin, 0, 2);
            this.tableMainLayout.Controls.Add(this.treeViewDestination, 1, 2);
            this.tableMainLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableMainLayout.Location = new System.Drawing.Point(0, 0);
            this.tableMainLayout.Name = "tableMainLayout";
            this.tableMainLayout.RowCount = 3;
            this.tableMainLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableMainLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableMainLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableMainLayout.Size = new System.Drawing.Size(819, 482);
            this.tableMainLayout.TabIndex = 5;
            // 
            // pnlLoad
            // 
            this.tableMainLayout.SetColumnSpan(this.pnlLoad, 2);
            this.pnlLoad.Controls.Add(this.btnDelete);
            this.pnlLoad.Controls.Add(this.lblSearch);
            this.pnlLoad.Controls.Add(this.txtBoxSearch);
            this.pnlLoad.Controls.Add(this.chkBoxSelectAll);
            this.pnlLoad.Controls.Add(this.btnClear);
            this.pnlLoad.Controls.Add(this.lblItemCount);
            this.pnlLoad.Controls.Add(this.btnLoad);
            this.pnlLoad.Controls.Add(this.txtItemCount);
            this.pnlLoad.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlLoad.Location = new System.Drawing.Point(3, 3);
            this.pnlLoad.Name = "pnlLoad";
            this.pnlLoad.Size = new System.Drawing.Size(813, 44);
            this.pnlLoad.TabIndex = 6;
            // 
            // lblSearch
            // 
            this.lblSearch.AutoSize = true;
            this.lblSearch.Location = new System.Drawing.Point(270, 16);
            this.lblSearch.Name = "lblSearch";
            this.lblSearch.Size = new System.Drawing.Size(44, 13);
            this.lblSearch.TabIndex = 8;
            this.lblSearch.Text = "Search:";
            // 
            // txtBoxSearch
            // 
            this.txtBoxSearch.Location = new System.Drawing.Point(317, 13);
            this.txtBoxSearch.Name = "txtBoxSearch";
            this.txtBoxSearch.Size = new System.Drawing.Size(126, 20);
            this.txtBoxSearch.TabIndex = 7;
            this.txtBoxSearch.TextChanged += new System.EventHandler(this.TxtBoxSearch_TextChanged);
            // 
            // chkBoxSelectAll
            // 
            this.chkBoxSelectAll.AutoSize = true;
            this.chkBoxSelectAll.Location = new System.Drawing.Point(533, 15);
            this.chkBoxSelectAll.Name = "chkBoxSelectAll";
            this.chkBoxSelectAll.Size = new System.Drawing.Size(70, 17);
            this.chkBoxSelectAll.TabIndex = 6;
            this.chkBoxSelectAll.Text = "Select All";
            this.chkBoxSelectAll.UseVisualStyleBackColor = true;
            this.chkBoxSelectAll.CheckedChanged += new System.EventHandler(this.chkBoxSelectAll_CheckedChanged);
            // 
            // btnClear
            // 
            this.btnClear.Location = new System.Drawing.Point(615, 11);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(75, 23);
            this.btnClear.TabIndex = 4;
            this.btnClear.Text = "Clear";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // lblFeedbackFast
            // 
            this.lblFeedbackFast.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblFeedbackFast.Location = new System.Drawing.Point(3, 50);
            this.lblFeedbackFast.Name = "lblFeedbackFast";
            this.lblFeedbackFast.Size = new System.Drawing.Size(403, 40);
            this.lblFeedbackFast.TabIndex = 7;
            this.lblFeedbackFast.Text = "TreeView Fast";
            this.lblFeedbackFast.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // treeViewOrigin
            // 
            this.treeViewOrigin.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeViewOrigin.Location = new System.Drawing.Point(3, 93);
            this.treeViewOrigin.Name = "treeViewOrigin";
            this.treeViewOrigin.Size = new System.Drawing.Size(403, 386);
            this.treeViewOrigin.TabIndex = 0;
            this.treeViewOrigin.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.TreeView_NodeMouseClick);
            // 
            // treeViewDestination
            // 
            this.treeViewDestination.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeViewDestination.Location = new System.Drawing.Point(411, 92);
            this.treeViewDestination.Margin = new System.Windows.Forms.Padding(2);
            this.treeViewDestination.Name = "treeViewDestination";
            this.treeViewDestination.Size = new System.Drawing.Size(406, 388);
            this.treeViewDestination.TabIndex = 8;
            this.treeViewDestination.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.TreeView_NodeMouseClick);
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(449, 11);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(56, 23);
            this.btnDelete.TabIndex = 9;
            this.btnDelete.Text = "Delete";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // Demo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(819, 482);
            this.Controls.Add(this.tableMainLayout);
            this.Name = "Demo";
            this.Text = "Tree load demo";
            this.Load += new System.EventHandler(this.Demo_Load);
            this.tableMainLayout.ResumeLayout(false);
            this.pnlLoad.ResumeLayout(false);
            this.pnlLoad.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Controls.TreeViewFast treeViewOrigin;
        private System.Windows.Forms.Button btnLoad;
        private System.Windows.Forms.Label lblItemCount;
        private System.Windows.Forms.TextBox txtItemCount;
        private System.Windows.Forms.TableLayoutPanel tableMainLayout;
        private System.Windows.Forms.Panel pnlLoad;
        private System.Windows.Forms.Label lblFeedbackFast;
        private Controls.TreeViewFast treeViewDestination;
        private System.Windows.Forms.CheckBox chkBoxSelectAll;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Label lblSearch;
        private System.Windows.Forms.TextBox txtBoxSearch;
        private System.Windows.Forms.Button btnDelete;
    }
}

